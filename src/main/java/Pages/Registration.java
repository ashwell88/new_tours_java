package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Instant;

public class Registration {
    //Create WebDriver variable
    private WebDriver driver;

    //Create constructor
    public Registration(WebDriver driver)
    {
        this.driver=driver;
    }
    //Locating Elements
    @FindBy(xpath = "//a[@href='register.php'][contains(.,'REGISTER')]")
    WebElement Register_link;

    @FindBy(xpath = "//input[contains(@name,'firstName')]")
    WebElement First_Name;

    @FindBy(xpath = "//input[contains(@name,'lastName')]")
    WebElement Last_Name;

    @FindBy(xpath = "//input[contains(@name,'phone')]")
    WebElement Phone;

    @FindBy(xpath = "//input[contains(@id,'userName')]")
    WebElement User_Email;

    @FindBy(xpath = "//input[contains(@name,'address1')]")
    WebElement Address;

    @FindBy(xpath = "//input[contains(@name,'city')]")
    WebElement City;

    @FindBy(xpath = "//input[contains(@maxlength,'40')]")
    WebElement State;

    @FindBy(xpath = "//input[contains(@name,'postalCode')]")
    WebElement Post_Code;

    @FindBy(xpath = "//select[contains(@name,'country')]")
    WebElement Country;

    @FindBy(xpath = "//input[contains(@id,'email')]")
    WebElement User_Name;

    @FindBy(xpath = "//input[contains(@name,'password')]")
    WebElement Password;

    @FindBy(xpath = "//input[contains(@name,'confirmPassword')]")
    WebElement Confirm_Password;

    @FindBy(xpath = "//input[@src='images/submit.gif']")
    WebElement Submit_Button;

    @FindBy(xpath = "//b[contains(.,'Note: Your user name is test123.')]")
    WebElement Registration_Message;

    //Create a method to click on the Register link
    public void Click_Register_Link()
    {
        //Waiting 30 secs for the WebElement to be visible
        WebDriverWait register=new WebDriverWait(driver, 30);
        register.until(ExpectedConditions.visibilityOf(Register_link));
        Register_link.click();
    }

    //Creating a method to enter first name
    public void First_Name(String firstName )
    {
        First_Name.sendKeys(firstName);
    }

    //Creating a method to enter last name
    public void Last_Name(String lastName)
    {
        Last_Name.sendKeys(lastName);
    }

    //Creating a method to enter phone number
    public void Phone(String phoneNumber)
    {
        Phone.sendKeys(phoneNumber);
    }

    public void User_Email(String userEmail)
    {
        User_Email.sendKeys(userEmail);
    }

    public void Address(String address)
    {
        Address.sendKeys(address);
    }

    public void City(String cityName)
    {
        City.sendKeys(cityName);
    }

    public void State(String stateName)
    {
        State.sendKeys(stateName);
    }

    public void Post_Code(String postCode)
    {
        Post_Code.sendKeys(postCode);
    }

    public void Country(String countryName)
    {
        Country.sendKeys(countryName);
        // Select _countryDrp=new Select(Country);
        //_countryDrp.selectByVisibleText(countryName);

    }

    public void User_Name(String userName)
    {
        User_Name.sendKeys(userName);
    }

    public void Password(String password)
    {
        Password.sendKeys(password);
    }

    public void Confirm_Password(String confirmPassword)
    {
        Confirm_Password.sendKeys(confirmPassword);
    }

    public void Submit_Button()
    {
        Submit_Button.click();
    }

    //Waiting 30 secs for the Registration message WebElement to be visible
    public void Registration_Message(String registerMessage)
    {
        WebDriverWait reg_message=new WebDriverWait(driver, 30);
        reg_message.until(ExpectedConditions.visibilityOf(Registration_Message));

        //Assert / Verify that the registration message is displayed
        Assert.assertTrue(Registration_Message.isDisplayed());
        //Comparing the message on test data to the results on the webpage
        //Assert.assertEquals(registerMessage,Registration_Message.getText());
    }


}
