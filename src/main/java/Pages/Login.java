package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Login {

    //We creating a instance of a driver with type WebDriver
    private WebDriver driver;

    //constructor to pass driver information t another class
    public Login(WebDriver driver) { this.driver=driver; }


    //Locating All the elements we need on the Pages.Login Page
    @FindBy(xpath = "//a[@href='login.php'][contains(.,'sign-in')]")
    WebElement Sign_ON_Link;

    @FindBy(xpath = "//input[contains(@name,'userName')]")
    WebElement Username;

    @FindBy(xpath = "//input[contains(@name,'password')]")
    WebElement Password;

    @FindBy(xpath = "//input[contains(@type,'submit')]")
    WebElement Submit_Button;

    @FindBy(xpath = "//h3[contains(.,'Login Successfully')]")
    WebElement Login_Message;

    //Create method to click the SIGN ON link

    public void Click_Sign_ON_Link()
    {
        //waiting 30 secs for the element to be visible
        WebDriverWait sign_on=new WebDriverWait(driver, 30);
        sign_on.until(ExpectedConditions.visibilityOf(Sign_ON_Link));

        Sign_ON_Link.click();
    }
    //Create a method to type in username
    public void Enter_username(String userName)
    {
        WebDriverWait username=new WebDriverWait(driver, 30);
        username.until(ExpectedConditions.visibilityOf(Username));

        Username.sendKeys(userName);
    }
    //Create a method to type in password
    public void Password(String passWord)
    {
        Password.sendKeys(passWord);
    }

    //create a method to click the submit button
    public void Click_Submit_Button()
    {
        Submit_Button.click();
    }
    //create a method to verify that a user is signed in

    public void Verify_successful_login()
    {
        WebDriverWait verify_login=new WebDriverWait(driver, 30);
        verify_login.until(ExpectedConditions.visibilityOf(Login_Message));

        //Assert / Verify that welcome message display
        Assert.assertTrue(Login_Message.isDisplayed());
        //Assert.assertEquals(  "Login Successfully", Login_Message.getText());

    }



}
