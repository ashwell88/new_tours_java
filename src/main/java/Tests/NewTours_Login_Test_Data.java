package Tests;


import Helpers.Reports_Class;
import Helpers.TakeScreenshot_Class;
import Pages.Home_Page_Login;
import Setup.Setup_Class;
import com.aventstack.extentreports.Status;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;

public class NewTours_Login_Test_Data extends Reports_Class {

    private WebDriver driver = Setup_Class.startBrowserofChoice("http://demo.guru99.com/test/newtours/", "chrome");
    Home_Page_Login login_ = PageFactory.initElements(driver, Home_Page_Login.class);
    TakeScreenshot_Class _screenShot = new TakeScreenshot_Class();
    private static final String excel = ".\\src\\main\\java\\Test_Data\\Login_Test_Data.xlsx";
    FileInputStream fis = new FileInputStream(excel);
    XSSFWorkbook workbook = new XSSFWorkbook(fis);
    XSSFSheet Login_Test_Data = workbook.getSheetAt(0);
    String Username = Login_Test_Data.getRow(1).getCell(0).getStringCellValue();
    String Password = Login_Test_Data.getRow(1).getCell(1).getStringCellValue();
    public NewTours_Login_Test_Data() throws IOException{

    }

    @Test()
    public void Login_User_Test() {
        test = extent.createTest("Login Test", "Login Registered User");
        test.log(Status.PASS, "Click Sign-On link");
        login_.Click_Sign_In_Link();
        test.log(Status.PASS, "Enter Username");
        login_.Enter_username_(Username);
        test.log(Status.PASS, "Enter Password");
        login_.Password_(Password);

        _screenShot.takeSnapShot(driver, "Login Test screen");

        test.log(Status.PASS, "Click Submit Button");
        login_.Click_Submit_Button();
        test.log(Status.PASS, "Very successful Login");
        login_.Verify_successful_login_();

        _screenShot.takeSnapShot(driver, "Successful Login Test");
    }
    //This is to close the browser
    @AfterSuite
    public void Quit()

    {
        driver.quit();
    }
}
