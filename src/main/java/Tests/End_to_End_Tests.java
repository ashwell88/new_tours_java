package Tests;

import Helpers.Reports_Class;
import Helpers.TakeScreenshot_Class;
import Pages.Login;
import Pages.Registration;
import Setup.Setup_Class;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

//Extend the report class so you can log the steps on the report
public class End_to_End_Tests extends Reports_Class {

    private WebDriver driver = Setup_Class.startBrowserofChoice("http://demo.guru99.com/test/newtours/", "chrome");
    Registration registration = PageFactory.initElements(driver, Registration.class);
    Login login = PageFactory.initElements(driver, Login.class);
    TakeScreenshot_Class _screenShot = new TakeScreenshot_Class();

    @Test(priority = 1)
    public void Register_User_Test()
    {
        test = extent.createTest("Register Class", "Register New User");
        test.log(Status.PASS, "Click Register Link");
        registration.Click_Register_Link();
        test.log(Status.PASS, "Input First Name");
        registration.First_Name("Test");
        test.log(Status.PASS, "Input Last Name");
        registration.Last_Name("Wellio");
        test.log(Status.PASS, "Input Phone Number");
        registration.Phone("0123654788");
        test.log(Status.PASS, "Enter email address");
        registration.User_Email("test@test.com");
        test.log(Status.PASS, "Enter Address");
        registration.Address("123 Test Road");
        test.log(Status.PASS, "Enter City");
        registration.City("Cape Town");
        test.log(Status.PASS, "State");
        registration.State("City");
        test.log(Status.PASS, "Enter Post Code");
        registration.Post_Code("7944");
        test.log(Status.PASS, "Select County from drop-down");
        registration.Country("SOUTH AFRICA");
        test.log(Status.PASS, "Enter Username");
        registration.User_Name("test123");
        test.log(Status.PASS, "Enter Password");
        registration.Password("test1234");
        test.log(Status.PASS, "Confirm Password");
        registration.Confirm_Password("test1234");
        test.log(Status.PASS, "Click Submit Button");
        registration.Submit_Button();
        registration.Registration_Message("");

    }
//Priority first than Alphabetically
    @Test(priority = 2)
    public void Login_User_Test() {
        test = extent.createTest("Login Class", "Login after Registration completed");
        test.log(Status.PASS, "Click Sign-in link");
        login.Click_Sign_ON_Link();
        test.log(Status.PASS, "Enter Username");
        login.Enter_username("test123");
        test.log(Status.PASS, "Password");
        login.Password("test1234");

        _screenShot.takeSnapShot(driver, "Login screen");

        test.log(Status.PASS, "Click Submit Button");
        login.Click_Submit_Button();
        test.log(Status.PASS, "Verify successful Login");
        login.Verify_successful_login();

        _screenShot.takeSnapShot(driver, "Successful Login");
    }
//This is to close the browser
    @AfterSuite
    public void Quit()

    {
        driver.quit();
    }

}

