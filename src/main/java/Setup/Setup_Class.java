package Setup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Setup_Class {

    public static final String Chromedriver=".\\src\\main\\java\\Drivers\\chromedriver.exe";
    public static final String Firefoxdriver="";
    public static WebDriver driver;

    public Setup_Class()
    {

    }

    public static WebDriver startBrowserofChoice(String url, String browserChoice)
    {
        if(browserChoice.equalsIgnoreCase("chrome"))
    {
           System.setProperty("webdriver.chrome.driver",Chromedriver);
           ChromeOptions options=new ChromeOptions();
           options.addArguments("--start-maximized");


           driver=new ChromeDriver(options);
      } else if (browserChoice.equalsIgnoreCase("firefox"))
        {
         //Launch Firefox driver
        }
      driver.get(url);
      return driver;
    }
}
